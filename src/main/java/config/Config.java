package config;

import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import service.TransferService;

@Configuration
public class Config {

    public TransferService transferService() {
        return new TransferService();
    }

    public JdbcTemplate getTemplate() {
        return new JdbcTemplate();
    }


}