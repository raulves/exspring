package service;

import lombok.Getter;

@Getter
public class TransferService {

    private BankService bankService;

    public void transfer(Integer amount, String fromAccount, String toAccount) {
        bankService.withdraw(amount, fromAccount);

        bankService.deposit(amount, toAccount);
    }
}