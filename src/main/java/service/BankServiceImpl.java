package service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class BankServiceImpl implements BankService {

    private static Logger logger = LogManager.getLogger(BankService.class);

    @Override
    public void deposit(Integer amount, String toAccount) {
        logger.debug("depositing ...");
    }

    @Override
    public void withdraw(Integer amount, String fromAccount) {
        logger.debug("withdrawing ...");
    }

}
